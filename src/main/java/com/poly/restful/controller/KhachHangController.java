package com.poly.restful.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.poly.restful.entity.KhachHang;
import com.poly.restful.service.KhachHangService;
import com.poly.restful.service.ServiceResult;

@RestController
@RequestMapping("/khachhang")
public class KhachHangController {
	
	@Autowired
	private KhachHangService khachhangService;
	
	@GetMapping("")
	public ResponseEntity<ServiceResult> findAllKhachHang(){
		System.out.println("Controller. method GET - findAll Khachhang");
			return  new ResponseEntity<ServiceResult>(khachhangService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ServiceResult> findByIDKH(@PathVariable int id){
		System.out.println("Controller. method GET- findById");
		return new ResponseEntity<ServiceResult>(khachhangService.findById(id), HttpStatus.OK);
		
	}
	@PostMapping("/dangky")
	public ResponseEntity<ServiceResult> findByTenAndSdt(@RequestParam String tenkhachhang, String sdt){
		System.out.println("Controller. method POST- findByTenAndSdt");
		return new ResponseEntity<ServiceResult>(khachhangService.findByTenAndSdt(tenkhachhang, sdt), HttpStatus.OK);
		
	}
	
//	@PostMapping("")
//	public ResponseEntity<ServiceResult> createKhachHang(@RequestBody KhachHang khachhang){
//		System.out.println("PostMapping - create Khachhhang");
//		return new ResponseEntity<ServiceResult>(khachhangService.create(khachhang), HttpStatus.OK);
//	}
	
	@PutMapping("")
	public ResponseEntity<ServiceResult> updateKhachHang(@RequestBody KhachHang khachhang){
		System.out.println("Update --KhachHang");
		return new ResponseEntity<ServiceResult>(khachhangService.update(khachhang), HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<ServiceResult> deleteKhachHang(@PathVariable int id){
		System.out.println("Delete --");
		return new ResponseEntity<ServiceResult>(khachhangService.deleteById(id), HttpStatus.OK);
		
	}
}
