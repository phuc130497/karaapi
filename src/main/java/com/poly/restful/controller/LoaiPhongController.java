package com.poly.restful.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.poly.restful.service.LoaiPhongService;
import com.poly.restful.service.ServiceResult;

@Controller
@RequestMapping("loaiphong")
public class LoaiPhongController {
	@Autowired
	LoaiPhongService loaiphongService;
	
	@GetMapping("")
	public ResponseEntity<ServiceResult> listAllLoaiPhong(){
		return new ResponseEntity<ServiceResult>(loaiphongService.finAllLoaiPhong(),HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ServiceResult> listByID(@PathVariable int id){
		return new ResponseEntity<ServiceResult>(loaiphongService.findByID(id),HttpStatus.OK);
	}
}
