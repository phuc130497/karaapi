package com.poly.restful.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.poly.restful.entity.KhachHang;
import com.poly.restful.service.KhachHangService;
import com.poly.restful.service.LoginService;
import com.poly.restful.service.ServiceResult;

@Controller

public class LoginController {
	@Autowired
	LoginService loginRepo;
	@Autowired
	KhachHangService khachhangService;
	
	@PostMapping("/loginkh")	
	public ResponseEntity<ServiceResult> loginKhachHang(@RequestParam String user, String pass){
		return  new ResponseEntity<ServiceResult>(loginRepo.loginKhachHang(user, pass), HttpStatus.OK);
	}
	@PostMapping("/loginnv")	
	public ResponseEntity<ServiceResult> logiNhanVien(@RequestParam String user, String pass){
		return  new ResponseEntity<ServiceResult>(loginRepo.loginNhanVien(user, pass), HttpStatus.OK);
	}
	@PostMapping("signupkh")
	public ResponseEntity<ServiceResult> createKhachHang(@RequestBody KhachHang khachhang){
		System.out.println("PostMapping - Sign Up KhachHang");
		return new ResponseEntity<ServiceResult>(loginRepo.signUpKhachHang(khachhang), HttpStatus.OK);
	}

}
