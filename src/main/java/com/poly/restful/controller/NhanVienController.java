package com.poly.restful.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.poly.restful.entity.NhanVien;
import com.poly.restful.service.NhanVienService;
import com.poly.restful.service.ServiceResult;

@RestController
@RequestMapping("/nhanvien")
public class NhanVienController {
	
	@Autowired
	private NhanVienService nhanvienService;
	
	@GetMapping("")
	public ResponseEntity<ServiceResult> findAllNhanVien(){
		System.out.println("Controller. method GET - findAllNhanVien");
			return  new ResponseEntity<ServiceResult>(nhanvienService.findAll(), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<ServiceResult> findByIdNV(@PathVariable int id){
		System.out.println("Controller. method GET- findById");
		return new ResponseEntity<ServiceResult>(nhanvienService.findById(id), HttpStatus.OK);
		
	}
	
	@PostMapping("")
	public ResponseEntity<ServiceResult> createUser(@RequestBody NhanVien nhanvien){
		System.out.println("PostMapping - create");
		return new ResponseEntity<ServiceResult>(nhanvienService.create(nhanvien), HttpStatus.OK);
	}
	
	@PutMapping("")
	public ResponseEntity<ServiceResult> updateUser(@RequestBody NhanVien nhanvien){
		System.out.println("Update --NhanVien");
		return new ResponseEntity<ServiceResult>(nhanvienService.update(nhanvien), HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<ServiceResult> deleteUser(@PathVariable int id){
		System.out.println("Delete --");
		return new ResponseEntity<ServiceResult>(nhanvienService.deleteById(id), HttpStatus.OK);
		
	}

}
