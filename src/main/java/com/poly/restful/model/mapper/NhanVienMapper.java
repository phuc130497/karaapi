package com.poly.restful.model.mapper;

import com.poly.restful.entity.NhanVien;
import com.poly.restful.model.dto.NhanVienDTO;

public class NhanVienMapper {
	public static NhanVienDTO toNhanVienDTO(NhanVien nv) {
		NhanVienDTO nvDTO = new NhanVienDTO();
		nvDTO.setId(nv.getId());
		nvDTO.setTennhanvien(nv.getTennhanvien());
		nvDTO.setUsername(nv.getUsername());
		nvDTO.setVaitro(nv.getVaitro());
		nvDTO.setHinhanh(nv.getHinhanh());
		
		return nvDTO;
	}
}
