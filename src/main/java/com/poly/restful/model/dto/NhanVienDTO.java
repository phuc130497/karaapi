package com.poly.restful.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NhanVienDTO {
	private int id;
	private String tennhanvien;
	private String username;
	private int vaitro;
	private String hinhanh;
}
