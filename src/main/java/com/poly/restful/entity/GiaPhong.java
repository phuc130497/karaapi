package com.poly.restful.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "giaphong")
public class GiaPhong {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idgiaphong")
	private int id;
	private String giaphong;
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern = "HH:MI:SS")
	private Date gio;
	
	@OneToMany(mappedBy = "giaphong")
	private Collection<ChiTietGiaPhong> ctgiaphong;
}
