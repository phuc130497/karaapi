package com.poly.restful.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "hoadon")
public class HoaDon {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idhoadon")
	private int id;
	
	@OneToOne
	@JoinColumn(name = "idoder")
	private Oder oder;
	
	private int tiencoc;
	private String hinhanhdatcoc;
	
	@ManyToOne
	@JoinColumn(name = "idnhanvienpv")
	private NhanVien nhanvienpv;
	
	@ManyToOne
	@JoinColumn(name = "idnhanvienlt")
	private NhanVien nhanvienlt;
	
	@OneToMany(mappedBy = "hoadon")
	private Collection<SuCo> suco;
	
	@OneToMany(mappedBy = "hoadon")
	private Collection<HoaDonChiTiet> hoadonchitiet;
	
}
