package com.poly.restful.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "oder")
public class Oder {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idoder")
	private int id;
	private boolean trangthai;
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "YYYY-MM-DD HH:MI:SS")
	private Date ngayoder;
	private String ghichu;
	@ManyToOne
	@JoinColumn(name = "idkhachhang")
	private KhachHang khachhang;
	
	@ManyToOne
	@JoinColumn(name = "idcaphong")
	private CaPhong caphong;
	
	@OneToMany(mappedBy = "oder")
	private Collection<OderDichVu> oderdichvu;
	
	@OneToOne(mappedBy = "oder")
	private HoaDon hoadon;
	
	
}
