package com.poly.restful.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "dichvu")
public class DichVu {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "iddichvu")
	private int id;
	private String tendichvu;
	private int giadichvu;
	private String hinhanh;
	private boolean trangthai;
	
	@OneToMany(mappedBy = "dichvu")
	private Collection<OderDichVu> oderdichvu;
	
	
}
