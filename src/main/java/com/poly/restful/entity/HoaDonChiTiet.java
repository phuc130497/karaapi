package com.poly.restful.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "hoadonchitiet")
public class HoaDonChiTiet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idhoadonct")
	private int id;
	private int sogio;
	private boolean trangthai;

	@ManyToOne
	@JoinColumn(name = "idhoadon")
	private HoaDon hoadon;
	
	private boolean loaithanhtoan;
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "YYYY-MM-DD HH:MI:SS")
	private Date ngaygiothanhtoan;
	private int tongchiphi;
	private String hinhanhthanhtoan;
	private String ghichu;
	
	@OneToMany(mappedBy = "hoadonct")
	private Collection<DanhGia> danhgia;
	
	
}
