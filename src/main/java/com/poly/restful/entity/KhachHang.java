package com.poly.restful.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "khachhang")
public class KhachHang {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idkhachhang")
	private int id;
	private String tenkhachhang;
	private String sdt;
	private String username;
	private String pass;
	
	public KhachHang(String tenkhachhang, String sdt) {
		super();
		this.tenkhachhang = tenkhachhang;
		this.sdt = sdt;
	}
	
	
	
//	@OneToMany(mappedBy = "khachhang")
//	private Collection<Oder> oder;
	
}
