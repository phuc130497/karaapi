package com.poly.restful.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "loaiphong")
public class LoaiPhong {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idloaiphong")
	private int id;
	private String tenloaiphong;
	private String ghichu;
	
//	@OneToMany(mappedBy = "loaiphong" ,fetch = FetchType.LAZY)
//	private Collection<Phong> phong;
	
	@OneToMany(mappedBy = "loaiphong" ,fetch = FetchType.LAZY)
	private Collection<ChiTietGiaPhong> chitietgiaphong;

}
