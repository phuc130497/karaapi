package com.poly.restful.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "nhanvien")
public class NhanVien {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idnhanvien")
	private int id;
	private String tennhanvien;
	private String username;
	private String pass;
	private int vaitro;
	private String sdt;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "YYYY-MM-DD")
	private Date ngaysinh;
	private String hinhanh;
	private String email;
	private boolean trangthai;
	
	//Note Join 2 column
//	@OneToMany(fetch = FetchType.LAZY,mappedBy = "nhanvienpv")
//	private Collection<HoaDon> hoadon1;
//	@OneToMany(fetch = FetchType.LAZY,mappedBy = "nhanvienlt")
//	private Collection<HoaDon> hoadon2;
	
}
