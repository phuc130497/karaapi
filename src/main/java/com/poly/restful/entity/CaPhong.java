package com.poly.restful.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "caphong")
public class CaPhong {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idcaphong")
	private int id;
	
//	@ManyToOne
//	@JoinColumn(name = "idCa")
//	private Ca ca;
//	
//	@ManyToOne
//	@JoinColumn(name = "idphong")
//	private Phong phong;
	
	private boolean trangthai;

//	@OneToMany(mappedBy = "caphong")
//	private Collection<CaPhong> caphong;
}
