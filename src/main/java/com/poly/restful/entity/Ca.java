package com.poly.restful.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "ca")
public class Ca {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idca")
	private int id;
	
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern = "HH:MI:SS")
	private Date giobatdau;
	
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern = "HH:MI:SS")
	private Date gioketthuc;
	
	@ManyToMany
	@JoinTable(name = "caphong", 
		joinColumns = @JoinColumn(name= "idCa"),
		inverseJoinColumns =  @JoinColumn(name = "idphong"))
	private Collection<Phong> phong;

}
