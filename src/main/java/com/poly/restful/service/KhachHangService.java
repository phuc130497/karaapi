package com.poly.restful.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poly.restful.entity.KhachHang;
import com.poly.restful.repository.KhachHangRepository;
import com.poly.restful.service.ServiceResult.Status;

@Service
public class KhachHangService {
	
	@Autowired
	KhachHangRepository khachHangRepo;
	
	public ServiceResult findAll() {
		ServiceResult result = new ServiceResult();
		result.setData(khachHangRepo.findAll());
		return result;
	}
	
	public ServiceResult findById(int id) {
		ServiceResult result = new ServiceResult();
		KhachHang khachhang = khachHangRepo.findById(id).orElse(null);
		if (khachhang == null) {
			result.setStatus(Status.FAILED);
			result.setMessage("Khach Hang Ko tồn tại");
		} else {
			result.setData(khachhang);
		}
		
		return result;
	}
//	public ServiceResult create(KhachHang khachhang) {
//		ServiceResult result = new ServiceResult();
//		KhachHang kh = khachHangRepo.findByTenkhachhangAndSdt(khachhang.getTenkhachhang(), khachhang.getSdt());
//		if (kh == null) {
//			result.setMessage("Đăng ký thành công!");
//			result.setData(khachHangRepo.save(khachhang));
//		} else {
//			result.setStatus(Status.FAILED);
//			result.setMessage("Đã Tồn tại Tài Khoản có TÊN và SĐT này!");
//		}
//		
//		return result;
//	}
	
	public ServiceResult update(KhachHang khachhang) {
		ServiceResult result = new ServiceResult();
		KhachHang kh = khachHangRepo.findById(khachhang.getId()).orElse(null);
		if (kh == null) {
			result.setStatus(Status.FAILED);
			result.setMessage("KhachHang ko tồn Tại");
		}else {
			result.setData(khachHangRepo.save(khachhang));
		}
		return result;
	}
	public ServiceResult deleteById(int id) {
		ServiceResult result = new ServiceResult();
		KhachHang kh = khachHangRepo.findById(id).orElse(null);
		if (kh == null) {
			result.setStatus(Status.FAILED);
			result.setMessage("KhachHang ko tồn Tại");
		} else {
			khachHangRepo.delete(kh);
			result.setMessage("Đã xóa Thành Công");
		}
		return result;
		
	}
	
	public ServiceResult findByTenAndSdt(String tenkhachhang, String sdt) {
		ServiceResult rs = new ServiceResult();
		KhachHang khachhang = new KhachHang(tenkhachhang, sdt);
		System.out.println("Service- Insert");
		KhachHang kh = khachHangRepo.findByTenkhachhangAndSdt(tenkhachhang, sdt);
		if (kh == null) {
			System.out.println("Service- Insert- KhachHang null");
			rs.setMessage("Thêm mới thành công!!!");
			rs.setData(khachHangRepo.save(khachhang));
		} else {
			System.out.println("Service- Insert- KhachHang not null");
			rs.setMessage("Lấy Tài Khoản có sẵn!!!");
			rs.setData(kh);
		}
		return rs;
	}
	
}
