package com.poly.restful.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ServiceResult {
	private Status status = Status.SUCCESS;
	private String message;
	private Object data;
	
	public enum Status {
	    SUCCESS, FAILED;
	  }
	public void exam() {
		
	}
}
