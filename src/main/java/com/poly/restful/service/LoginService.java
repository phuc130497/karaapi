package com.poly.restful.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poly.restful.entity.KhachHang;
import com.poly.restful.entity.NhanVien;
import com.poly.restful.repository.KhachHangRepository;
import com.poly.restful.repository.NhanVienRepository;
import com.poly.restful.service.ServiceResult.Status;

@Service
public class LoginService {

		@Autowired
		NhanVienRepository nhanVienRepo;
		@Autowired
		KhachHangRepository khachHangRepo;
		
	public ServiceResult loginNhanVien(String user, String pass) {
		ServiceResult rs =new ServiceResult();
		NhanVien nv = nhanVienRepo.findByUsername(user);
		if (nv == null) {
			rs.setStatus(Status.FAILED);
			rs.setMessage("TaiKhoan Ko ton tai");
			
		} else if(!nv.getPass().equals(pass)){
			rs.setStatus(Status.FAILED);
			rs.setMessage("Sai Mat Khau !!!!");
		} else {
			rs.setMessage("Login Ok");
			rs.setData(nv);
		}
		return rs;
	}
	
	public ServiceResult loginKhachHang(String user, String pass) {
		ServiceResult rs =new ServiceResult();
		KhachHang kh = khachHangRepo.findByUsername(user);
		if (kh == null) {
			rs.setStatus(Status.FAILED);
			rs.setMessage("TaiKhoan Ko ton tai");
			
		} else if(!(kh.getPass().equalsIgnoreCase(pass))){
			rs.setStatus(Status.FAILED);
			rs.setMessage("Sai Mat Khau !!!!");
		} else {
			rs.setMessage("Login Ok");
			rs.setData(kh);
		}
		return rs;
	}
	
	private boolean checkUsername(String username) {
		KhachHang khachhang = khachHangRepo.findByUsername(username);
		if (khachhang == null) {
			return false;
		} 
		
		return true;
	}
	public ServiceResult signUpKhachHang(KhachHang khachhang) {
			ServiceResult result = new ServiceResult();
			KhachHang kh = khachHangRepo.findByTenkhachhangAndSdt(khachhang.getTenkhachhang(), khachhang.getSdt());
			if (checkUsername(khachhang.getUsername())) {
				result.setMessage("UserName đã sử dụng");
				result.setStatus(Status.FAILED);
			}else if (!(kh == null)) {
				result.setStatus(Status.FAILED);
				result.setMessage("Đã Tồn tại Tài Khoản có TÊN và SĐT này!");
			} 
			result.setMessage("Thêm Mới thành công!");
			result.setData(khachHangRepo.save(khachhang));
			return result;

	}
	
}
