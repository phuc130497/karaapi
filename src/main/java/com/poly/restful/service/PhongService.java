package com.poly.restful.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poly.restful.entity.Phong;
import com.poly.restful.repository.PhongRepository;
import com.poly.restful.service.ServiceResult.Status;

@Service
public class PhongService {
	
	@Autowired
	PhongRepository phongRepo;
	
	public ServiceResult findAllPhong() {
		ServiceResult rs = new ServiceResult();
		rs.setData(phongRepo.findAll());
		return rs;
	}
	
	public ServiceResult findById(int id) {
		ServiceResult rs = new ServiceResult();
		Phong phong = phongRepo.findById(id).orElse(null);
		if (phong==null) {
			rs.setStatus(Status.FAILED);
			rs.setMessage("Phong ko ton tai");
		} else {
			rs.setData(phong);
		}
		return rs;
	}
	
	public ServiceResult findByIdLoaiPhong(int idloaiphong) {
		ServiceResult rs = new ServiceResult();
		rs.setData(phongRepo.findByIdloaiphong(idloaiphong));
		return rs;
	}
}
