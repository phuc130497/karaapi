package com.poly.restful.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poly.restful.entity.NhanVien;
import com.poly.restful.repository.NhanVienRepository;
import com.poly.restful.service.ServiceResult.Status;

@Service
public class NhanVienService {
	
	@Autowired
	NhanVienRepository nhanVienRepo;
	
	public ServiceResult findAll() {
		ServiceResult result = new ServiceResult();
		result.setData(nhanVienRepo.findAll());
		return result;
	}
	
	public ServiceResult findById(int id) {
		ServiceResult result = new ServiceResult();
		NhanVien nhanvien = nhanVienRepo.findById(id).orElse(null);
		if (nhanvien == null) {
			result.setStatus(Status.FAILED);
			result.setMessage("Nhân Viên Ko tồn tại");
		} else {
			result.setData(nhanvien);
		}
		
		return result;
	}
	private boolean checkUsername(String username) {
		NhanVien nhanvien = nhanVienRepo.findByUsername(username);
		if (nhanvien == null) {
			return false;
		} 
		
		return true;
	}

	public ServiceResult create(NhanVien nhanvien) {
		ServiceResult result = new ServiceResult();
		if (checkUsername(nhanvien.getUsername())) {
			result.setMessage("UserName da su dung");
			result.setStatus(Status.FAILED);
		}
		result.setMessage("Thêm Mới thành công!");
		result.setData(nhanVienRepo.save(nhanvien));
		return result;
	}
	public ServiceResult update(NhanVien nhanvien) {
		ServiceResult result = new ServiceResult();
		NhanVien nv = nhanVienRepo.findById(nhanvien.getId()).orElse(null);
		if (nv == null) {
			result.setStatus(Status.FAILED);
			result.setMessage("Nhân Viên ko tồn Tại");
		} else {
			result.setData(nhanVienRepo.save(nhanvien));
		}
		return result;
	}
	public ServiceResult deleteById(int id) {
		ServiceResult result = new ServiceResult();
		NhanVien nv = nhanVienRepo.findById(id).orElse(null);
		if (nv == null) {
			result.setStatus(Status.FAILED);
			result.setMessage("Nhân Viên Ko tồn tại");
		} else {
			nhanVienRepo.delete(nv);
			result.setMessage("Đã xóa Thành Công");
		}
		return result;
		
	}
}
