package com.poly.restful.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poly.restful.entity.LoaiPhong;
import com.poly.restful.repository.LoaiPhongRepository;
import com.poly.restful.service.ServiceResult.Status;

@Service
public class LoaiPhongService {
	@Autowired
	LoaiPhongRepository loaiPhongRepo;
	
	public ServiceResult finAllLoaiPhong() {
		ServiceResult rs = new ServiceResult();
		rs.setData(loaiPhongRepo.findAll());
		return rs;
	}
	public ServiceResult findByID(int id) {
		ServiceResult rs = new ServiceResult();
		LoaiPhong loaiphong = loaiPhongRepo.findById(id).orElse(null);
		if (loaiphong == null) {
			rs.setStatus(Status.FAILED);
			rs.setMessage("LoaiPhong ko ton tai!!!");
		} else {
			rs.setData(loaiphong);
		}
		return rs;
	}
}
