package com.poly.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.poly.restful.entity.LoaiPhong;

@Repository
public interface LoaiPhongRepository extends JpaRepository<LoaiPhong, Integer>{

}
