package com.poly.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.poly.restful.entity.Phong;

@Repository
public interface PhongRepository extends JpaRepository<Phong, Integer>{
	
	Phong findByIdloaiphong(int idloaiphong);
}
