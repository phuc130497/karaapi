package com.poly.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.poly.restful.entity.KhachHang;

@Repository
public interface KhachHangRepository extends JpaRepository<KhachHang, Integer> {
	KhachHang findByUsername(String username);
	
	@Query(value = "insert into khachhang (tenkhachhang, sdt) values(=?1 , =?2) ", nativeQuery = true)
	KhachHang insertTenAndSdt(String tenkhachhang, String sdt);
	
	KhachHang findByTenkhachhangAndSdt(String tenkhachhang, String sdt);
	
}
